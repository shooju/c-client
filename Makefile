
CC=gcc -std=c99
LIBS=`pkg-config --libs openssl` -levent
OBJS=shooju.o date_utils.o
LDFLAGS=-static -Wl

libshooju.a: $(OBJS)
	ar rcs $@ $(OBJS)
