# libshooju c-client

*libshooju* is the official C client library for Shooju with the following features:

* Authentication via username and api key
* Getting series and fields

# Building

## Linux/Mac OS

1. Install openssl development package
2. Clone libevent library from https://github.com/a212/libevent.git to `../libevent`
3. Build libevent with standard `./configure;make;make install`
4. Build library with `make`

## Windows

1. Install ActivePerl (for building openssl library) from https://www.activestate.com/activeperl/downloads/
2. Download and extract openssl source from https://www.openssl.org/source/ to `../openssl`
3. Configure openssl library.  Use `perl Configure VC-WIN32 no-asm no-shared` for 32-bit and `perl Configure VC-WIN64A no-asm no-shared` for 64-bit
4. Clone libevent library from https://github.com/a212/libevent.git to `../libevent`
5. Execute in VS command prompt `build.bat`

# Usage

		#include "stdio.h"
		#include "shooju.h"
		#ifdef _WIN32
		#include <winsock2.h>
		#endif

		int main(int argc, char *argv[])
		{
			struct shooju *s = shooju_init("https://example.shooju.com", "api_key", "user_name");
		#ifdef _WIN32
			WSADATA wsa_data;
			WSAStartup(0x0201, &wsa_data);
		#endif
			shooju_set_date_format(s, sdf_iso);
			shooju_set_location_type(s, "Your application name");
			if (!shooju_series_search(s, "query", "", 0, "2012-02-29", "MAX", 100)) {
				printf("Result: %s", shooju_get_body(s));
			}
			shooju_done(s);
		}

## Build for linux

		gcc example.c `pkg-config --libs openssl` -levent -lshooju

## Build for windows

		cl example.c libshooju.lib ../libevent/libevent.lib wsock32.lib Ws2_32.lib ../openssl/libssl.lib ../openssl/libcrypto.lib ../libevent/libevent_openssl.lib crypt32.lib Advapi32.lib user32.lib
