
#include <stdlib.h>
#include "shooju.h"

static int isLeap(int year)
{
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);;
}

static int getExtraDays(int year)
{
	return (year / 4) - (year / 100) + (year / 400);
}

const int64_t epoch1970 = 62167132800;
static int month_days[] = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
static int month_leap_days[] = { 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335 , 366};

static const char *rd(const char *buf, int *val, int cnt, char last) {
	if (!buf) return NULL;
	for (int i = 0; i < cnt; i++) {
		if (buf[i] > '9' || buf[i] < '0') return NULL;
		*val = *val * 10 + (buf[i] - '0');
	}
	return buf[cnt] == last ? (buf + cnt + 1) : NULL;
}

int64_t iso_to_milli(const char *value)
{
	int64_t milli;
	int msec = 0, hour = 0, min = 0, sec = 0;
	int year = 0, month = 0, day = 0;
	rd(rd(rd(rd(rd(rd(rd(value, &year, 4, '-'), &month, 2, '-'), &day, 2, 'T'), &hour, 2, ':'), &min, 2, ':'), &sec, 2, '.'), &msec, 3, 0);
	if (month > 12 || month < 1) return -1;
	month--;
	milli = 365 * year + getExtraDays(year - 1);
	milli = (milli + (isLeap(year) ? month_leap_days : month_days)[month] + day - 1) * 24;
	milli = ((milli + hour) * 60 + min) * 60 + sec;
	milli = (milli - epoch1970) * 1000 + msec;
	return milli;
}

static char *wr(char *buf, int val, int cnt, char last) {
	for (int i = 1; i <= cnt; i++) {
		buf[cnt - i] = (val % 10) + '0';
		val /= 10;
	}
	buf[cnt] = last;
	return buf + cnt + 1;
}

void milli_to_iso(char *buf, int64_t value)
{
	value += epoch1970 * 1000;
	int msec = value % 1000;
	value /= 1000;
	int sec = value % 60;
	value /= 60;
	int min = value % 60;
	value /= 60;
	int hour = value % 24;
	value /= 24;
	int year = (int)value / 365;
	int days = (int)value % 365;
	for (;;) {
		int extra_days = getExtraDays(year - 1);
		if (extra_days > days) {
			days += 365;
			year--;
		} else {
			days -= extra_days;
			break;
		}
	}
	int month = 0;
	int *md = (isLeap(year) ? month_leap_days : month_days);
	while (days >= md[month + 1]) month++;
	days -= md[month];
	wr(wr(wr(wr(wr(wr(wr(buf, year, 4, '-'), month + 1, 2, '-'), days + 1, 2, 'T'), hour, 2, ':'), min, 2, ':'), sec, 2, '.'), msec, 3, 0);
}
