
#include "stdafx.h"
#include "../shooju.h"

int64_t iso_to_milli_os(char *value)
{
	int64_t milli;
	int msec = 0;
	struct tm tm;
	memset(&tm, 0, sizeof(tm));
	sscanf(value, "%04d-%02d-%02dT%02d:%02d:%02d.%03d", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &msec);
	tm.tm_mon--;
	tm.tm_year -= 1900;
	tm.tm_isdst = 0;
	milli = _mktime64(&tm);
	milli -= _timezone;
	milli = milli * 1000 + msec;
	return milli;
}

void milli_to_iso_os(char *buf, int64_t milli)
{
	time_t t = milli / 1000;
	strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%S", gmtime(&t));
}

void test1()
{
	std::cout << "Checking milli_to_iso and iso_to_milli..." << std::endl;
	char buf[64];
	int cnt = 0;
	for (int64_t i = -315619200000; i < -215619200000; i += 30000) {
		milli_to_iso(buf, i);
		if (iso_to_milli(buf) != i) {
			printf("Error in reverse conversion! for %s\n", buf);
			return;
		} else {
			//printf("%s,  %" PRId64 "\n", buf, i);
		}
		cnt++;
	}
	printf("Correct! Checked %d dates\n", cnt);
}

void test2()
{
	std::cout << "Checking iso_to_milli..." << std::endl;
	char buf[64];
	for (int64_t i = 10000000000; i < 50000000000; i += 30000) {
		milli_to_iso(buf, i);
		int64_t os = iso_to_milli_os(buf);
		if (i != os) {
			printf("Missmatch with OS conversion! for %s  %" PRId64 " != %" PRId64 "\n", buf, i, os);
			return;
		}
	}
	std::cout << "iso_to_milli is correct!" << std::endl;
	milli_to_iso(buf, iso_to_milli("1965-05-03"));
	if (strcmp(buf, "1965-05-03T00:00:00.000")) {
		std::cout << "Error iso_to_milli with partial date!!!" << std::endl;
	}
}

void test3()
{
	std::cout << "Benchmarking..." << std::endl;
	using namespace std::chrono;
	int N = 100000;
	char *dates[] = { "1970-08-10T10:20:30.234", "1985-01-17T12:18:15.523", "2000-12-31T10:11:44.921", nullptr };
	int64_t sum = 0;
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (int i = 0; i < N; i++) {
		for (char **d = dates; *d; d++) {
			sum += iso_to_milli(*d);
		}
	}
	duration<double> time_span = duration_cast<duration<double>>(high_resolution_clock::now() - t1);
	std::cout << "iso_to_milli    : " << sum << " takes " << time_span.count() << " seconds." << std::endl;
	t1 = high_resolution_clock::now();
	sum = 0;
	for (int i = 0; i < N; i++) {
		for (char **d = dates; *d; d++) {
			sum += iso_to_milli_os(*d);
		}
	}
	time_span = duration_cast<duration<double>>(high_resolution_clock::now() - t1);
	std::cout << "iso_to_milli(os): " << sum << " takes " << time_span.count() << " seconds." << std::endl;

	char buf[64];
	t1 = high_resolution_clock::now();
	for (int i = 0; i < N; i++) {
		milli_to_iso(buf, i);
	}
	time_span = duration_cast<duration<double>>(high_resolution_clock::now() - t1);
	std::cout << "milli_to_iso    : " << time_span.count() << " seconds." << std::endl;

	t1 = high_resolution_clock::now();
	for (int i = 0; i < N; i++) {
		milli_to_iso_os(buf, i);
	}
	time_span = duration_cast<duration<double>>(high_resolution_clock::now() - t1);
	std::cout << "milli_to_iso(os): " << time_span.count() << " seconds." << std::endl;
}

int main()
{
	test1();
	test2();
	test3();
	return 0;
}

