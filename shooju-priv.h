#ifndef _SHOOJU_PRIV_H
#define _SHOOJU_PRIV_H

struct event_base;
struct ssl_ctx_st;

struct requestParam
{
	char *name;
	char *value;
	struct requestParam *next;
};

struct shooju {
	char *server_url;
	char *api_key;
	char *user;
	char *location_type;
        char *receive_format;
        char *send_format;

	struct event_base *evbase;
	struct evhttp_connection *evcon;
	struct bufferevent *bev;

	struct ssl_ctx_st *ssl_ctx;

	char *host;
	int port;
	int is_ssl;
	int retries;
	int retry_idx;

	int res_code;
	char *body;
	size_t body_len;
	char *request_url;
	int is_post_request;

	void (*cb)(struct evhttp_request *, void *);
	void *ctx;

	enum shooju_date_format date_format;
	int query_size;
	struct requestParam *request_params;
};

#endif // _SHOOJU_PRIV_H