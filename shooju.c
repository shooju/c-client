#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "shooju.h"
#include "shooju-priv.h"

#define REQUEST_BUFFER_SIZE 1024

#ifdef _WIN32
#define strdup _strdup
#define snprintf _snprintf
#define strcasecmp _stricmp
#define itoa _itoa
#else
extern char *strdup(const char *s); // POSIX
 int strcasecmp(const char *s1, const char *s2);
#endif

#define EVENT__HAVE_OPENSSL

#include <event2/bufferevent_ssl.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/http.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

static char *shooju_date_format_strings[] = {
	"unix",
	"milli",
	"iso"
};

static int shooju_retry_delays[] = {
	4, 8, 15, 30
};

#define SHOOJU_RES_OK     0
#define SHOOJU_RES_INIT   (-1)
#define SHOOJU_RES_BEV    (-2)
#define SHOOJU_RES_EVCON  (-3)
#define SHOOJU_RES_NEW    (-4)
#define SHOOJU_RES_REQ    (-5)
#define SHOOJU_RES_SSL    (-6)
#define SHOOJU_RES_SOCKET (-7)


static void 
init_ssl()
{
	static int done = 0;
	if (!done) {
		done = 1;
		SSL_library_init();
		ERR_load_crypto_strings();
		SSL_load_error_strings();
		OpenSSL_add_all_algorithms();
	}
}

static void
die_openssl(const char *func)
{
	fprintf(stderr, "%s failed:\n", func);

	/* This is the OpenSSL function that prints the contents of the
	* error stack to the specified file handle. */
	ERR_print_errors_fp(stderr);

}

struct shooju *
shooju_init(const char *server_url, const char *api_key, const char *user)
{
	struct event_base *ev = event_base_new();
	if (!ev) {
		perror("event_base_new()");
	}
	return shooju_init_async(server_url, api_key, user, ev, NULL, NULL);
}

struct shooju *
shooju_init_async(const char *server_url, const char *api_key, const char *user, struct event_base *ev, void(*cb)(struct evhttp_request *req, void *ctx), void *ctx)
{
	int fail = 0;
	struct shooju *s = (struct shooju *)calloc(1, sizeof(struct shooju));
	struct evhttp_uri *http_uri;
	const char *scheme;
	char *s_prefix = (strchr(server_url, ':') ? "" : "https://");
	char *s_suffix = (strchr(server_url, '.') ? "" : ".shooju.com");

	init_ssl();
	
	s->server_url = (char*)malloc(strlen(s_prefix) + strlen(server_url) + strlen(s_suffix) + 1);
	strcat(strcat(strcpy(s->server_url, s_prefix), server_url), s_suffix);

	s->api_key = strdup(api_key);
	s->user = strdup(user);
	s->location_type = strdup("c-client");
	s->receive_format = NULL;
	s->send_format = NULL;


	s->evbase = ev;
	s->cb = cb;
	s->ctx = ctx;
	s->body = NULL;
	s->request_url = NULL;
	s->request_params = NULL;

	http_uri = evhttp_uri_parse(s->server_url);
	if (http_uri == NULL) {
		perror("malformed url");
		fail = 1;
	}

	scheme = evhttp_uri_get_scheme(http_uri);
	s->is_ssl = (scheme && strcasecmp(scheme, "https") == 0);
	if (scheme == NULL || (!s->is_ssl && strcasecmp(scheme, "http") != 0)) {
		perror("url must be http or https");
		fail = 1;
	}

	s->host = strdup(evhttp_uri_get_host(http_uri));
	if (s->host == NULL) {
		perror("url must have a host");
		fail = 1;
	}

	s->port = evhttp_uri_get_port(http_uri);
	if (s->port == -1) {
		s->port = s->is_ssl ? 443 : 80;
	}

	if (s->is_ssl) {
		/* Create a new OpenSSL context */
		s->ssl_ctx = SSL_CTX_new(SSLv23_method());
		if (!s->ssl_ctx) {
			die_openssl("SSL_CTX_new");
			fail = 1;
		}
	}

	s->query_size = 10;

	if (fail) {
		shooju_done(s);
		s = NULL;
	}

	return s;
}

void
shooju_set_date_format(struct shooju *s, enum shooju_date_format date_format)
{
	s->date_format = date_format;
}

void
shooju_set_query_size(struct shooju *s, int query_size)
{
	s->query_size = query_size;
}

void
shooju_set_api_key(struct shooju *s, const char *api_key)
{
	free(s->api_key);
	s->api_key = strdup(api_key);
}

void
shooju_set_location_type(struct shooju *s, const char *location_type)
{
	free(s->location_type);
	s->location_type = strdup(location_type);
}

void
shooju_set_receive_format(struct shooju *s, const char *receive_format)
{
	free(s->receive_format);
	s->receive_format = strdup(receive_format);
}

void
shooju_set_send_format(struct shooju *s, const char *send_format)
{
	free(s->send_format);
	s->send_format = strdup(send_format);
}

void
shooju_add_param(struct shooju *s, const char *name, const char *value)
{
	struct requestParam *p = (struct requestParam *)malloc(sizeof(struct requestParam));
	struct requestParam **last = &s->request_params;
	p->name = evhttp_encode_uri(name);
	p->value = evhttp_encode_uri(value);
	p->next = NULL;
	while (*last) last = &(*last)->next;
	*last = p;
}

void
shooju_done(struct shooju *s)
{
	if (!s) {
		return;
	}

	shooju_close(s);

	if (s->evbase && !s->cb) {
		event_base_free(s->evbase);
	}

	free(s->body);
	free(s->request_url);
	free(s->user);
	free(s->api_key);
	free(s->server_url);
	free(s->location_type);

	free(s);
}

void
shooju_close(struct shooju *s)
{
	if (s && s->evcon) {
		evhttp_connection_free(s->evcon);
		s->evcon = NULL;
	}
}

static void
http_request_done(struct evhttp_request *req, void *ctx)
{
	char buffer[256];

	struct shooju *s = (struct shooju *)ctx;
	struct evbuffer *buf;

	if (req == NULL) {
		/* If req is NULL, it means an error occurred, but
		* sadly we are mostly left guessing what the error
		* might have been.  We'll do our best... */
		unsigned long oslerr;
		int printed_err = 0;
		int errcode = EVUTIL_SOCKET_ERROR();
		fprintf(stderr, "some request failed - no idea which one though!\n");
		/* Print out the OpenSSL error queue that libevent
		* squirreled away for us, if any. */
		while ((oslerr = bufferevent_get_openssl_error(s->bev))) {
			ERR_error_string_n(oslerr, buffer, sizeof(buffer));
			fprintf(stderr, "%s\n", buffer);
			printed_err = 1;
			s->res_code = SHOOJU_RES_SSL;
		}
		/* If the OpenSSL error queue was empty, maybe it was a
		* socket error; let's try printing that. */
		if (!printed_err) {
			fprintf(stderr, "socket error = %s (%d)\n",
			evutil_socket_error_to_string(errcode),
			errcode);
			s->res_code = SHOOJU_RES_SOCKET;
		}
		return;
	}
	s->res_code = evhttp_request_get_response_code(req);
#if 0
	fprintf(stderr, "Response line: %d %s\n", s->res_code,
		evhttp_request_get_response_code_line(req));
#endif
	buf = evhttp_request_get_input_buffer(req);
	s->body_len = evbuffer_get_length(buf);
	s->body = (char*)malloc(s->body_len + 1);
	evbuffer_remove(buf, s->body, s->body_len);
	s->body[s->body_len] = 0; // end of line
}

static char encoding_table[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
'w', 'x', 'y', 'z', '0', '1', '2', '3',
'4', '5', '6', '7', '8', '9', '+', '/' };
static char *decoding_table = NULL;
static int mod_table[] = { 0, 2, 1 };

char *base64_encode(const char *prefix, const char *data,
	size_t input_length,
	size_t *output_length) {

	*output_length = 4 * ((input_length + 2) / 3) + strlen(prefix) + 1;

	char *encoded_data = (char*)malloc(*output_length);
	if (encoded_data == NULL) return NULL;
	strcpy(encoded_data, prefix);

	for (size_t i = 0, j = strlen(prefix); i < input_length;) {

		uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
		uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
		uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

		uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

		encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
	}

	for (int i = 0; i < mod_table[input_length % 3]; i++)
		encoded_data[*output_length - 2 - i] = '=';
	encoded_data[*output_length - 1] = 0;
	return encoded_data;
}

static int 
_do_request(struct shooju *s, char *request, char *data, size_t data_sz)
{
	struct evhttp_request *req;
	struct evkeyvalq *output_headers;
	if (!s || !s->evbase) {
		fprintf(stderr, "not initialized or evbase is invalid\n");
		return SHOOJU_RES_INIT;
	}

	s->res_code = SHOOJU_RES_OK;
	s->retry_idx = 0;
	s->is_post_request = data ? 1 : 0;
#if 0
	fprintf(stderr, "request: %s\n", request);
#endif
	for(;;) {
		shooju_close(s); // TODO: reuse connection?

		if (s->body) {
			free(s->body);
			s->body = NULL;
			s->body_len = 0;
		}

		if (s->is_ssl) {
			// Create OpenSSL bufferevent and stack evhttp on top of it
			SSL *ssl = SSL_new(s->ssl_ctx);
			if (ssl == NULL) {
				die_openssl("SSL_new()");
			}

#ifdef SSL_CTRL_SET_TLSEXT_HOSTNAME
			// Set hostname for SNI extension
			SSL_set_tlsext_host_name(ssl, s->host);
#endif
			s->bev = bufferevent_openssl_socket_new(s->evbase, -1, ssl,
				BUFFEREVENT_SSL_CONNECTING,
				BEV_OPT_CLOSE_ON_FREE/* | BEV_OPT_DEFER_CALLBACKS*/);
		} else {
			s->bev = bufferevent_socket_new(s->evbase, -1, BEV_OPT_CLOSE_ON_FREE);
		}

		if (s->bev == NULL) {
			fprintf(stderr, "bufferevent_openssl_socket_new() failed\n");
			s->res_code = SHOOJU_RES_BEV;
			break;
		}

		bufferevent_openssl_set_allow_dirty_shutdown(s->bev, 1);

		s->evcon = evhttp_connection_base_bufferevent_new(s->evbase, NULL, s->bev,
			s->host, s->port);
		if (s->evcon == NULL) {
			fprintf(stderr, "evhttp_connection_base_bufferevent_new() failed\n");
			s->res_code = SHOOJU_RES_EVCON;
			break;
		}

		evhttp_connection_set_timeout(s->evcon, 600);
		if (s->retries > 0) {
			evhttp_connection_set_retries(s->evcon, s->retries);
		}

		// Fire off the request
		req = evhttp_request_new(s->cb ? s->cb : http_request_done, s);
		if (req == NULL) {
			fprintf(stderr, "evhttp_request_new() failed\n");
			s->res_code = SHOOJU_RES_NEW;
			break;
		}

		output_headers = evhttp_request_get_output_headers(req);
		evhttp_add_header(output_headers, "Host", s->host);
		evhttp_add_header(output_headers, "Connection", "close");
		if (s->receive_format) {
			evhttp_add_header(output_headers, "Sj-Receive-Format", s->receive_format);
		}
		if (s->send_format) {
			evhttp_add_header(output_headers, "Sj-Send-Format", s->send_format);
		}

		char buf[128];
		evutil_snprintf(buf, sizeof(buf) - 1, "%s:%s", s->user, s->api_key);
		size_t b64sz;
		char *b64 = base64_encode("Basic ", buf, strlen(buf), &b64sz);
		evhttp_add_header(output_headers, "Authorization", b64);
		free(b64);

		if (data) {
			struct evbuffer *output_buffer = evhttp_request_get_output_buffer(req);
			evbuffer_add(output_buffer, data, data_sz);
			evutil_snprintf(buf, sizeof(buf) - 1, "%lu", (unsigned long)data_sz);
			evhttp_add_header(output_headers, "Content-Type", "application/json");
			evhttp_add_header(output_headers, "Content-Length", buf);
		}


		int r = evhttp_make_request(s->evcon, req, data ? EVHTTP_REQ_POST : EVHTTP_REQ_GET, request);

		if (!s->cb) {
			event_base_dispatch(s->evbase);
		}

		if ((r || s->res_code < 0 || s->res_code >= 500) && s->retry_idx < sizeof(shooju_retry_delays) / sizeof(int)) {
			struct timeval delay = { shooju_retry_delays[s->retry_idx++] };
			evutil_usleep_(&delay);
			continue;
		}

		if (r != 0) {
			fprintf(stderr, "evhttp_make_request() failed\n");
			s->res_code = SHOOJU_RES_REQ;
			break;
		}

		break;
	}
	free(s->request_url);
	s->request_url = request;
	return s->res_code > 0 ? 0 : s->res_code;
}

static int 
do_request(struct shooju *s, char *request, char *data)
{
	size_t sz = 0;
	if (data) sz = strlen(data);
	return _do_request(s, request, data, sz);
}

static char *
shooju_req_header(struct shooju *s, const char *method, char **req) {
	char *fmt = "/api/1/%s?location_type=%s";
	size_t sz = strlen(fmt) + strlen(method) + strlen(s->location_type);
	*req = (char*)malloc(REQUEST_BUFFER_SIZE + sz);
	snprintf(*req, sz, fmt, method, s->location_type);
	return *req + strlen(*req);
}

int
shooju_authenticate(struct shooju *s)
{
	char *fmt = "/api/1/authenticate/?location_type=%s%c{\"user\":\"%s\",\"password\":\"%s\"}";
	size_t sz = strlen(fmt) + strlen(s->api_key) + strlen(s->user);
	char *req = (char*)malloc(sz);
	snprintf(req, sz, fmt, s->location_type, 0, s->user, s->api_key);
	return do_request(s, req, req + strlen(req) + 1);
}

int
shooju_ping(struct shooju *s)
{
	char *req;
	shooju_req_header(s, "status/ping", &req);
	return do_request(s, req, NULL);
}

static int
_do_request_with_params(struct shooju *s, const char *method, char *data, size_t data_sz)
{
	char *req;
	shooju_req_header(s, method, &req);
	struct requestParam *p = s->request_params;
	while (p) {
		struct requestParam *t = p;
		snprintf(req + strlen(req), REQUEST_BUFFER_SIZE, "&%s=%s", t->name, t->value);
		p = p->next;
		free(t->name);
		free(t->value);
		free(t);
	}
	s->request_params = NULL;
	return _do_request(s, req, data, data_sz);
}

static int
do_request_with_params(struct shooju *s, const char *method)
{
	return _do_request_with_params(s, method, NULL, 0);
}

int
shooju_series(struct shooju *s)
{
	struct requestParam *p = s->request_params;
	int hasScrollId = 0, hasDateFormat = 0;
	while (p) {
		if (strcmp(p->name, "scroll_id") == 0) hasScrollId = 1;
		else if (strcmp(p->name, "date_format") == 0) hasDateFormat = 1;
		p = p->next;
	}
	if (!hasScrollId && !hasDateFormat) shooju_add_param(s, "date_format", shooju_date_format_strings[s->date_format]);
	return do_request_with_params(s, "series");
}

int 
shooju_series_search(struct shooju *s, const char *query, const char *fields, int scroll, const char *date_min, const char *date_max, int size)
{
	char buf[32];
	shooju_add_param(s, "query", query);
	itoa(s->query_size, buf, 10);
	shooju_add_param(s, "scroll_batch_size", buf);
	shooju_add_param(s, "fields", fields);
	shooju_add_param(s, "date_format", shooju_date_format_strings[s->date_format]);
	shooju_add_param(s, "df", (!date_min || !*date_min) ? "MIN" : date_min);
	shooju_add_param(s, "dt", (!date_max || !*date_max) ? "MAX" : date_max);
	shooju_add_param(s, "scroll", scroll ? "true" : "false");
	itoa(size, buf, 10);
	shooju_add_param(s, "max_points", buf);

	return do_request_with_params(s, "series");
}

int 
shooju_series_scroll(struct shooju *s, const char *scroll_id)
{
	shooju_add_param(s, "scroll_id", scroll_id);

	return do_request_with_params(s, "series");
}

int
shooju_series_raw_post(struct shooju *s, const char *url, void *data, size_t data_sz)
{
	return _do_request_with_params(s, url[0]=='/' ? (url + 1) : url, data, data_sz);
}

int
shooju_series_write(struct shooju *s, int job_id, void *data, size_t data_sz)
{
	char *fmt = "/api/1/series/write?job_id=%d";
	size_t sz = strlen(fmt) + 16;
	char *req = (char*)malloc(sz);
	snprintf(req, sz, fmt, job_id);
	return _do_request(s, req, data, data_sz);
}

int
copy_escaped(char *buf, const char *str)
{
	int res = 0;
	if (buf) buf[res] = '"';
	res++;
	while (str) {
		const char *t;
		if (t = strchr(str, '"')) {
			if (buf) memcpy(buf + res, str, t-str);
			res += t-str;
			if (buf) buf[res] = '\\';
			res++;
			if (buf) buf[res] = '"';
			res++;
			str = t + 1;
		} else if (t = strchr(str, '\\')) {
			t++;
			if (buf) memcpy(buf + res, str, t-str);
			res += t-str;
			if (buf) buf[res] = '\\';
			res++;
			str = t;
		} else {
			int sz =  strlen(str);
			if (buf) memcpy(buf + res, str, sz);
			res += sz;
			break;
		}
	}
	if (buf) {
		buf[res] = '"';
		buf[res + 1] = 0;
	}
	res++;
	return res;
}

int
shooju_register_job(struct shooju *s, const char *description)
{
	char *fmt = "/api/1/jobs%c{\"source\":\"%s\",\"notes\":\"\",\"description\":"; 
	size_t sz = strlen(fmt) + strlen(s->location_type) + copy_escaped(NULL, description);
	char *req = (char*)malloc(sz);
	char *t = req + snprintf(req, sz, fmt, 0, "api"/*s->location_type*/);// FIXME use real location type
	t += copy_escaped(t, description);
	*t++ = '}';
	*t++ = 0;
	shooju_set_receive_format(s, NULL);
	return do_request(s, req, req + strlen(req) + 1);
}

int
shooju_finish_job(struct shooju *s, int job_id)
{
	char *fmt = "/api/1/jobs/%d/finish%c{}";
	size_t sz = strlen(fmt) + 16;
	char *req = (char*)malloc(sz);
	snprintf(req, sz, fmt, job_id, 0);
	shooju_set_receive_format(s, NULL);
	return do_request(s, req, req + strlen(req) + 1);
}

char *
shooju_get_body(struct shooju *s)
{
	return (s ? s->body : NULL);
}

size_t
shooju_get_body_len(struct shooju *s)
{
	return (s ? s->body_len : 0);
}

char *
shooju_get_request_url(struct shooju *s)
{
	return (s ? s->request_url : NULL);
}

int 
shooju_get_res_code(struct shooju *s)
{
	return (s ? s->res_code : 0);
}

int
shooju_is_post_request(struct shooju *s)
{
	return (s ? s->is_post_request : 0);
}

void *
shooju_get_ctx(struct shooju *s)
{
	return s->ctx;
}
