#ifndef _SHOOJU_H
#define _SHOOJU_H

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

struct shooju;
struct event_base;
struct evhttp_request;
enum shooju_date_format {
	sdf_unix,
	sdf_milli,
	sdf_iso
};

struct shooju *shooju_init(const char *server_url, const char *api_key, const char *user);
struct shooju *shooju_init_async(const char *server_url, const char *api_key, const char *user, struct event_base *ev, void (*cb)(struct evhttp_request *, void *), void *ctx);
void shooju_done(struct shooju *s);
void shooju_close(struct shooju *s);

void shooju_set_date_format(struct shooju *s, enum shooju_date_format date_format);
void shooju_set_query_size(struct shooju *s, int query_size);
void shooju_set_api_key(struct shooju *s, const char *api_key);
void shooju_set_location_type(struct shooju *s, const char *location_type);
void shooju_set_receive_format(struct shooju *s, const char *receive_format);
void shooju_set_send_format(struct shooju *s, const char *send_format);
void shooju_add_param(struct shooju *s, const char *name, const char *value);

int shooju_authenticate(struct shooju *s);
int shooju_ping(struct shooju *s);
int shooju_series(struct shooju *s);
int shooju_series_search(struct shooju *s, const char *query, const char *fields, int scroll, const char *date_min, const char *date_max, int size);
int shooju_series_scroll(struct shooju *s, const char *scroll_id);
int shooju_series_write(struct shooju *s, int job_id, void *data, size_t data_sz);
int shooju_series_raw_post(struct shooju *s, const char *url, void *data, size_t data_sz);
int shooju_register_job(struct shooju *s, const char *description);
int shooju_finish_job(struct shooju *s, int job_id);

char *shooju_get_body(struct shooju *s);
size_t shooju_get_body_len(struct shooju *s);
char *shooju_get_request_url(struct shooju *s);
int shooju_get_res_code(struct shooju *s);
void *shooju_get_ctx(struct shooju *s);
int shooju_is_post_request(struct shooju *s);

int64_t iso_to_milli(const char *value);
void milli_to_iso(char *buf, int64_t value);

#ifdef __cplusplus
}
#endif

#endif // _SHOOJU_H